from django.shortcuts import render



def home(request):
    data = dict(course='Course', course_detail='Course Detail', about='About', contact='Contact')
    return render(request, 'home.html', data)


def course(request):
    cours = dict(python='Python', django='Django', java='Java')
    return render(request, 'course.html', cours)


def detail(request):
    return render(request, 'detail.html')


def about(request):
    return render(request, 'about.html')


def contact(request):
    return render(request, 'contact.html')
